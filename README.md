# **TALLER 5 - FUERZA BRUTA - HEURISTICA** <br/>
Sudoku <br/>

## Presentado por :star2:

* Juan Sebastián Barreto Jiménez
* Janet Chen He

## Objetivo
Escribir formalmente e implementar dos algoritmos que solucionen un problema superpolinomial.

## Descripción
El problema a trabajar en este taller es "solucionar un Sudoku".

Para esto, usted debe crear un programa en su lenguaje favorito que:

1. Reciba por línea de comandos el nombre de un archivo (ejemplos) donde este la configuración inicial del tablero.
2. Informe por pantalla el tablero resuelto y el tiempo que tomó cada algoritmo en resolverlo.

## Estructura de Archivos

1. game_00.txt -> Contine el tablero 00 en .txt
2. game_00.txt -> Contine el tablero 00 en .txt
3. game_00.txt -> Contine el tablero 00 en .txt
4. game_00.txt -> Contine el tablero 00 en .txt
5. game_00.txt -> Contine el tablero 00 en .txt
6. sudoku.py -> Contiene la implementación del taller 5
7. Taller_5__Dos_algoritmos_para_solucionar_un_Sudoku.pdf -> Contine el documento en .pdf
8. tallerCinco.tex -> Contine el documento en .tex
