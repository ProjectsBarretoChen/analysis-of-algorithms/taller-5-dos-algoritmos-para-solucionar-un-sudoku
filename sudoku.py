import time
import sys
from pprint import pprint

def read(archive):
    with open(archive, 'r') as f:
        board = []
        for line in f:
            row = []
            if line[-1] == '\n':
                line = line[:-1]
            for c in line:
                if c.isdigit():
                    row.append(int(c))
                else:
                    row.append(0)
            board.append(row)
    return board

def solveBruteForce(board):
    if not findEmpty(board):
        return True
    row, col = findEmpty(board)
    for num in range(1, 10):
        if isValid(board, row, col, num):
            board[row][col] = num
            if solveBruteForce(board):
                return True
            board[row][col] = 0
    return False

def solveGreedy(board):
    if not findEmpty(board):
        return True
    row, col = findEmpty(board)
    possible_values = getPossibleValues(board, row, col)
    for num in possible_values:
        if isValid(board, row, col, num):
            board[row][col] = num
            if solveGreedy(board):
                return True
            board[row][col] = 0
    return False

def sortLength(elem):
    return len(elem[2])

def solveHeuristic(board):
    if not findEmpty(board):
        return True
    row, col = findEmpty(board)
    possible_values = getPossibleValues(board, row, col)
    empty_cells = [(row, col, possible_values)]
    empty_cells.sort(key=sortLength)
    for cell in empty_cells:
        row, col, possible_values = cell
        for num in possible_values:
            if isValid(board,row, col, num):
                board[row][col] = num
                if solveHeuristic(board):
                    return True
                board[row][col] = 0
    return False

def findEmpty(board):
    for row in range(9):
        for col in range(9):
            if board[row][col] == 0:
                return row, col
    return None

def getPossibleValues(board, row, col):
    possible_values = set(range(1, 10))
    possible_values -= set(board[row])
    possible_values -= set([board[i][col] for i in range(9)])
    box_row = (row // 3) * 3
    box_col = (col // 3) * 3
    possible_values -= set(
        [board[i][j] for i in range(box_row, box_row + 3) for j in range(box_col, box_col + 3)]
    )
    return possible_values

def isValid(board, row, col, val):
    if val in board[row]:
        return False
    if val in [board[i][col] for i in range(9)]:
        return False
    row_start, col_start = (row // 3) * 3, (col // 3) * 3
    for i in range(row_start, row_start + 3):
        for j in range(col_start, col_start + 3):
            if board[i][j] == val:
                return False
    return True


if len(sys.argv) == 2:
    archive = sys.argv[1]
else:
    print("Error - Introduce los argumentos correctamente")
    print('Ejemplo: sudoku.py game_00.txt')

board = read(archive)

print("Tablero inicial:")
pprint(board)

def typeSolve(solution):
    board = read(archive)
    print('----------------------------------')
    if(solution == 'BruteForce'):
        print('----------- BruteForce -----------')
        start_time = time.time()
        solveBruteForce(board)
        end_time = time.time()
    elif(solution == 'Greedy'):
        print('------------- Greedy -------------')
        start_time = time.time()
        solveGreedy(board)
        end_time = time.time()
    elif(solution == 'Heuristic'):
        print('------------ Heuristic -----------')
        start_time = time.time()
        solveHeuristic(board)
        end_time = time.time()

    print("Tablero resuelto:")
    pprint(board)

    print("Tiempo de ejecución: {:.3f} segundos".format(end_time - start_time))

typeSolve('BruteForce')
typeSolve('Greedy')
typeSolve('Heuristic')

